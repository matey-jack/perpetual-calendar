# cal

Weekdays fall on different dates in each year. 
So if you like a printed calendar on your desk or wall, you need to get a new one for each year. 
A **perpetual calendar**, however, can show the correct day of week for any day in any year.

This project allows you to print + cut + assemble your own rotatable perpetual calendar. 
Work in progress!

Here is the current on-line demo: http://perpetual-calendar.surge.sh/

- on mobile, you can use touch to turn the discs! (Mouse drag coming soon.)
- but the best thing: hit Ctrl-P or the print button in your browser to print  
  several sheets of paper which you can cut and glue together to get a 
  fully-functional hardware-only no-electricity needed perpetual calendar!

And here some early (but working) drafts for printing and assembly manual: 
[drive.google.com/~](https://drive.google.com/drive/folders/1H_XcwEy9ss2iJw-3ykmv9hnS-K80vPyF?usp=sharing)

Lots of early sketches:
[photos.google.com/~](https://photos.google.com/share/AF1QipOfeLNkucSvfFfn3Aary816Q_dj8RgLMrPbWhuCR1UI9yQOXhlQe51My3L2RTLQXQ?key=em5SRXVla3psRUxjRFdueFdPZHVsamhCbHR6Y0Jn)

Perpetual calendars like this have existed since the 20th century, but I found a 
way to make them a lot nicer, so I created this project to build it.

![Simple perpetual calendar cast in sheet metal](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/50yearcalendar.JPG/320px-50yearcalendar.JPG) 
[From Wikipedia](https://en.wikipedia.org/wiki/File:50yearcalendar.JPG)

What motivated me to build a round calendar with a different underlying mechanism:
 - My scheme clearly **shows which year and month are currently selected**. This makes
   it prettier and also useful as a reminder of the current time of year. (And you can
   easily add a pointer to the current day, too.)
 - Months and years are ordered sequentially on their discs, so it is **much easier to
   find and select a desired month and year**. Switching from one year or month to the 
   next or previous one is always just one move to the next item on the ring. That's 
   not just **very user-friendly**, but also a nice allusion to the circularity of life.
 - It's nice to see all the months in a circle which completes a whole year. 
   (We could even add a mechanism which switches the year when you go between
   January and December.)
 - We can easily add more features like **highlighting holidays** only for the current
   month or showing a seasonal background image. (Technique see below.) Both of
   this works offline on paper!
 - Unlike the 50-year calendar shown above which also has a (even more chaotic)
   variant with closer to (or more than) one-hundred years, this calendar here can
   easily add mechanics to **scale to several hundred years** and still keep the above
   property of **only and cleanly showing the current year**.


# Variants

There are currently four variants planned for a 28 year calendar and then various ideas 
for an "all of Gregorian history and future calendar".

The four variants differ an the following two axes:
 - rectangular frame (most simply one A4 sheet or whatever paper you use for printing) 
   vs round calendar with no frame.
   * The round calendar has the day-of-week ring on the outside and can use it as turning handle.
     It also offers seasonal illustrations in the center (see below).
   * The framed calendar has the months and year on the outside and is generally suited for 
     people who like framed things because the frame has lots of upright (that is: not turning)
     space for decoration with renaissance or Victorian or any other kind of neo-historic styling.
 - floating parts (which require two disks to be connected while a third one is between them 
   and free to turn vs all-on-one-axis which is much easier to build.
   
I currently only have one variant, by already a lot of infrastructure in place to make the 
others possible with a lot of shared code. There is still a bit more rewriting and refactoring to
do and then the other three variants are going to be quite easy, haha!        
 
## Upright calendars: stands on a table or hangs on a wall.

This is the first variant currently implemented. It's coolest feature is that 
the sheet with months is cut on the inside and the outside can have any shape 
(square or rectangular just like the paper you print on). This sheet can be 
attached (with glue or tape) to a back-plate (of cardboard, plywood, plastic, 
or even metal) to get a solid and representative construction which will we be
durable for the 28 years of the calendar's basic validity.

Cool features:
 - year is printed in big digits on top, which makes it very representative.
 - with the daynumbers between the months and days-of-week, it is very easy to 
   read. For example, "Wednesday, 1st of August 2018" is read from the inside 
   disc towards the outside. 

There are a few options in this variant which we might later integrate via
switches in the UI. For now, the following only serves as description of what's 
there and why and what might be coming some time.

Where to put the duplicated months (leap vs common years)
 * all left of year window: (CURRENTLY IMPLEMENTED)
   - leaves more space for the window, 
   - maybe prettier to some people
 * common year left, leap year right: (NOT going to implement this any time soon)
   - window has just enough space, 
   - provides more space for the leap-year indicator which can be just between 
     the affected months
   
Order of sheets / rings:
  * daynumber-ring between months and weekdays:  (CURRENTLY IMPLEMENTED)
    - is prettier, 
    - but requires "floating" construction, see below
  * or daynumber-ring in the centre: 
    - not as pretty, because day-of-week is not shown right next to daynumber
    - but easier to build as it can be held together with a central screw, bolt, 
      nut, rivet, button, or whatever you have at home
    - call it "the single pivot variant"

The single pivot variant can be made with a very slim day-of-week ring which only 
uses colored marks for the weekends and mid-week (see the granny-calendar). It's an 
attractive option to build because it's the only single-pivot construction of the three
upright variants.


## Handheld calendars, aka Kid's variant, aka seasonal variant

Here we have just three rings, no backplate, and just one central bolt around 
which everything turns.

I call it Kid's version, because it is so easy to build.

"Handheld version" because the year-window is turning with the rings which a user intuitively 
makes up for by turning the entire calendar in their hands. This is the opposite
of the upright-property of the previous variant.

Finally seasonal variant, because wie can make a big pie-piece-shaped window into
the center of the daynumbers-disk and show a seasonal illustration on the month-disc underneath. 
This can even be one big
circular picture which always shows a different part depending on the selected 
month. (Window width can be an entire season. A quarter of 35 is 8.75 slots, 
but we can do more or less depending on artistic ideas.)

Order of rings:
 * Underneath everything a sheet with the days-of-week on the very outside 
   (also forms the outer edge of the entire calendar)
   -  on that same sheet a ring of years just inside of the days-of-week
 * middle sheet has the months and a window for the years 
   (ordered just like the upright version)
 * inner sheet with day-numbers and flapsicle to select the current month.

I previously also called this "anniversary variant" because we could have small 
windows which highlight special dates in specific months. For example, when your
birthmonth is selected, a little heart appears on your birthday! I thought this
was really cool for reminding all the birthdates of a family or maybe also public
holidays, but it turned out, that this doesn't scale well: there will we hearts
popping up at random dates as soon as we punch more than one hole in the disk.
So it could still be cool for an ego-centric person to mark their own birthday,
for a couple to mark their lover's day or for a company or other organisation to
mark one specific day. But it's not a priorized variant in my backlog any more.  

## Upright variant of the seasonal variant

This is the one, I want to implement next!

Uses same print-outs as handheld, but the months (middle layer) are fixed on 
a backplate. The outer most printed layer of weekdays and years is floating
between the middle layer and the backplate.

This arrangement is nice, because it can stand on a desk or hang on a wall 
and is "frameless": ring with days-of-week is its outermost part and the weekends
and mid-week points can be highlighted with the shape of the ring without any 
marks on the paper at all. This ring then also serves as its own handle!

## Multiple centuries calendar

Here we can turn one more disc to select the century and the "generation" 
(group of 28 years). This calendar is the one which offers real perpetuity 
because it can not only cover the entire lifetime of all the people you've
known and ever will know, but also the entire history since the Gregorian
Calendar was started in October 1582 plus a few hundred years in the future.

Order of discs:
 * middle and top most disc is a simple cover with a big window for the year
   (with century). 
   - This disc can be fixed to a backplate in the center, with all other discs
     rotating around this wide axis. (Similar to floating discs in other variants).
   - big window is two slots wide (one for century, one for generation year) and
     four rows high (because there are four year rings, one for each generation).
 * month disc carries the centuries, with a little window for the generation
   besides the century. Each century is repeated four times, each one above a 
   different generation-year ring. This way, we can just turn one ring and switch
   between centuries and between generations at the same time.
 * day-of-week disc carries the four generation-year rings.
 * outer most (and bottom-most) disc has day-numbers and the month-selection
   flapsicle which all my calendars have.

Variants:
 * months and days-of-weeks can swap their position in which case we combine
   days-of-week with the centuries and year-windows and the months with the four
   generation years. Will decide for one variant before actually building this.
 * Building with 42 slots instead of 35 will require only three rings for the
   generation-year and also leaves more room for different centuries. A bigger
   calendar like that is also better to brag about!
 * Writing century and generation year above one-another instead of side-by-side
   allows the big window to be only one slot wide, therefore allowing more 
   centuries to be covered.

Time-span is between 300 years (35 slots century left of year) and 800 years 
(42 slots, century above year) when not using other tricks to increase it even 
more. Note that the former should just cover "lifetime of all the people you'll
ever know" and the latter covers all history from 1566 to 2366. You may now 
contemplate the likelihood that the Gregorian calendar will not be used any more 
in the 24th century. :-)

# Wie man es bastelt: die Schwimmende (floating) Variante

Scheiben-Stapelung von außen nach innen:
 - Monate mit Jahresfenster fest auf der Hintergrundplatte
 - Jahreszahlen unter dem Fenster
 - Tageszahlen schwimmend
 - Wochentage (verbunden mit Jahreszahlen innerhalb des Schwimmrings)

Stapelung von oben nach unten:
 - Monate + Wochentage beide ganz oben, da keine Überlappung
 - Tageszahlen
 - Jahreszahlen
 - Hintergrundplatte

Bastelanleitung: zuerst Jahre, Tagesnummern und Monate zusammenfügen, indem man
erstere und letztere verklebt während Tagesnummern beweglich dazwischen eingespannt sind.

# Development notes

Setting up dev environment. I use IntelliJ IDEA which automatically downloads the dart SDK.
    
    pub global activate webdev
    
Running the app:

    webdev serve

To run all unit tests use:

    pub run test -p chrome
    
This is needed because SVG DOM only works in a real browser. But all non-SVG unit
tests also run directly in IntelliJ...

Running all tests which do not need a browser:

    pub run test
    
Interestingly this uses the Dart-VM instead of JavaScript, 
so is much faster because nothing needs to be transpiled.
(The tool calls it "compile to JavaScript".)
