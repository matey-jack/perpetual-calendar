
Done and works:
===============
 - Monatsring
 - Jahresbox
 - An- und Aus-Schalten der verschiedenen Ringe
 - Jahreszahlenring
 - korrekte Jahreszahlen
 - PDF-Druck + media-queries
 - Drucke Ringe auf verschiedenen Seiten
 - Tagesnummern-Scheibe
 - Monate Jan/Feb für Gemeinjahre
 - DezJanFeb-Fenster-Mär Variante, damit Feb und Mär nicht im selben Slot
   liegen. (Ist so gleichmäßiger und aktueller Monat ist eindeutig ablesbar.)
   ==> Milestone: volles Bastelset
 - circles for cutting
 - fourth page for printing
 - "LEAP" anstatt des Pfeils im Jahresfenster und bei den beiden leap months
   ==> Milestone: volles Bastelset, dass auch wirklich funktioniert
 - outer circle for year-sheet (page 2), because it has to be larger than month-ring
 - bigger year number!  =>  works, but needs to be adjusted to make "leap" text fit
   --> enlarge window towards outside; move "leap" towards outside CHECK
   --> also center window and adjust years accordingly  CHECK
   --> replace outer circle by arc with gap at window  CHECK
 - two rectangles at well-chosen z-index hide all the years CHECK
 - add 6 small-ticks in February so it doesn't appear to have 7×n + 2 days.
 - replace layer view controls by a single button which hides month layer and hiders.
 - Hintergrundfarbe für Tagesnummernring 
 - title on calendar front (web version)
 - nice flapsicle (Path with arcs and straight lines)
 - Daynumbers start rotated to current month 
 - rotate Daynumbers with mouse wheel (select month)
 - rotate years with mousewheel
 - rotate using touch (drag/drop) 
   ==> Milestone: funktionaler Kalender on-line
 - snap daynumbers to valid month onTouchEnd, including correct common/leap month
 - extract first component class (MonthRingGenerator)
 - model state for selected year, because that is needed to snap on leap months correctly!
 - drag and turn years with touch and snap
==> Milestone: fachlich ziemlich robuster Kalender on-line, der auf dem Handy 
    schon richtig was her macht.

 - rotation center is now circle center :-D

 - implement mouse click and drag (includes OngoingDrag abstraction for touch 
   and mouse, but don't try to make the perfect abstraction yet. prio should 
   be to just get it working so we can go on with other important things.)

in progress:
============
 
 - /print/ show header only on top page

bugs:
=====

ready-to-implement backlog:
===========================

online:
 - show A4 frame with cut-out for grab-doors on both sides
 - drag years on grab-doors instead of view-windows
  ==> this will be an online-variant that I can actually share widely!
   
 - create PathBuilder and use it in both variants
 - use SlottedCircle and other helpers in both variants
 - Jessie's ears 
 ==> those are the prerequisites to redo the entire calendar in a more flexible way

paper-world:
 - print weekdays and months on same sheet (innermost and outermost parts shall have same color;
   if user doesn't want it, they can hand-color or print the sheet twice.)
 - markings of connection-flaps
 - side-window in month-sheet (page 1) so year-sheet can be grabbed and moved
 - at least some kind of dashed grey marker to show where to not cut the handle off
 - three scissor symbols per circle for showing what to cut out
 - glue symbol or text "put glue here up to the dashed line"
 - Calendar Title
   + only print on front page
   + good vertical spacing

===> Milestone: version 1 of printable wall calendar ready to share with the world.
--> looking forward to physically build a few of those to get the hang of it and
    collect experience...

instructions:
 - mention to cut outside of lines so parts move freely 
   --> or print an extra-line to make daynumbers-disk smaller by design in the print version
 - add pictures

next big project: merge grandfather-style and working calendar, offer some options 
to switch between variants using same code base
  ==> because I want to physically build it and keep it on my desk!



small improvements web-world (no priority at the moment):

 - reset button to go back to current year + month ("reset to May 2018")
    * with animation :-P  --> same effect already achieved by reload.

 - when moving month disc between Jan and Dec, year-disc should move at same speed
   one year forward or backward, example: from Dec 2015 to Jan 2016 and vice-versa.
   --> nice gimmick, but does not align with our plan to have web-world be a demo
       and simulation of paper world.

 - Hintergrundfarbe für Jahresring, inclusive
   + der Fixierlaschen, die vorne herausschauen
   + der Überlappung unter den Monaten, wenn "look inside"
   + der ganzen Scheibe in der Mitte, wenn die Wochentage bei "look inside" auch ausgeblendet sind.

  ==> ggf. mehrere Farbschemata ausdenken. Drei Farben:
    + Grundfarbe für Rahmen und Mitte (90% der Fläche, also eher dezent)
    + Tagesnummernring und Flapsicle 
    + Jahresscheibe, Fixierlaschen, seitlicher Drehgriff
      
 - Rückseite mit allen Jahrhunderten:
   - online "umdrehen", ggf. durch einen Klick auf ein Fenster, dieses nach vorne zaubern
   - in der Druckvorlage einrichten

 - Kreisscheibensegmet für Hinterlegung Sa/So --> geht genauso wie das Jahresfenster.


zukünftige Richtungen (je nach Feedback & muss noch durchdacht werden):
=======================================================================

 - andere Sprachen
 - zweites Fenster für spätes 19. Jahrhundert.
 - andere Form der Fenster (für Monate und Jahre, z.B. rundlich und nach außen offen)

Candy:
- SVG Export with minimal server-side code: https://d3export.housegordon.org/
or like this: https://gist.github.com/mbostock/6466603


