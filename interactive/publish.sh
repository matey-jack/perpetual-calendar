#!/usr/bin/env bash
set -e
set -x

# test files have annotations for VM and browser, so each of the following
# runs that subset of tests
pub run test -p vm
pub run test -p chrome

dart compile js web\main.dart -o web\main.dart.js
dart compile js web\granny.dart -o web\granny.dart.js

# alternatively, try:
dart run build-runner serve

# and remember to occasionally dart format and dart fix.

# google-chrome web/index.html
