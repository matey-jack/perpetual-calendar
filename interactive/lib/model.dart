import 'dart:math';

const weekdays = const ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

const _monthNames = const [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];

/// takes a month between 1 and 12
String getMonthName(int month) => _monthNames[month - 1];

/// `generationYear` should be between 0 and 27
int getSlotForYear(int generationYear) =>
    generationYear + (generationYear ~/ 4);

/// `slot` can be any angle in slots, even wrapped several times around.
/// result will be between 0 and 27
int getNearestYear(double slot) {
  final double s = slot % 35;
  // evenly splitting the leap-year gap takes a little effort
  final int r = ((s + 1) % 5 - 1).round();
  return ((s + 1) ~/ 5) * 4 + max(0, min(3, r));
}

/// `month` is one-based just like the result of `DateTime.month`.
/// Result is between 0 and 34 inclusive.
int getMonthStart(int month, bool leap) =>
    _monthStarts[month] - (month <= DateTime.february && leap ? 1 : 0);

bool isLeapYear(int year) =>
    (year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0));

/// `s` can be any angle in slots, even wrapped several times around.
/// The result is always a month between 1 and 12 inclusive.
int getNearestMonth(double s, bool leap) {
  s %= 35;

  int upper = DateTime.december;
  // invariant: s <= _getMonthStart(upper, leap)
  // initially true because s <= 35
  while (s < getMonthStart(upper - 1, leap)) {
    upper -= 1;
  }
  // In addition to the invariant, we now know
  //      _getMonthStart(upper - 1, leap) <= s
  // so both of the following difference are >= 0.
  int previous = upper == 1 ? 12 : upper - 1;
  return (s - getMonthStart(previous, leap)) < (getMonthStart(upper, leap) - s)
      ? previous
      : upper;
}

// One-based list where index = month and value = slot.
// Values for Jan and Feb are the common-year position.
// To make reverse-lookup excitingly simple, we have two guard elements:
// index [0] is 0 and index [12] is 35.
final _monthStarts = _initMonthStarts();

List<int> _initMonthStarts() {
  // First delta is from 0 to the common-year January.
  const _monthDeltas = const [2, 3, 7, 3, 2, 3, 2, 3, 3, 2, 3, 2];

  // TODO: this can be done functionally with something like Haskell's "scan"
  final result = <int>[0];
  for (int slot = 0, month = 1; month <= 12; month++) {
    slot += _monthDeltas[month - 1];
    result.add(slot);
  }
  return result;
}

void main() {
  for (var m = DateTime.january; m <= DateTime.december; m++) {
    if (m <= DateTime.february) {
      print("${getMonthName(m)} (leap): ${getMonthStart(m, true)}");
    }
    print("${getMonthName(m)}: ${getMonthStart(m, false)}");
  }
  /* just for debugging / reference:
      Jan (leap): 1
      Jan: 2
      Feb (leap): 4
      Feb: 5
      Mar: 12
      Apr: 15
      May: 17
      Jun: 20
      Jul: 22
      Aug: 25
      Sep: 28
      Oct: 30
      Nov: 33
      Dec: 35
   */
}
