import 'dart:math';
import 'dart:svg' hide Point;

import 'package:cal/math.dart';

Point? center(CircleElement e) {
  final x = e.cx?.baseVal?.value;
  final y = e.cy?.baseVal?.value;

  if (x != null && y != null) {
    return Point(x, y);
  }
  return null;
}

num? radius(CircleElement e) => e.r?.baseVal?.value;

LineElement line(Point a, Point b) {
  const fractionDigits = 4;
  return new LineElement()
    ..attributes = {
      "x1": a.x.toStringAsFixed(fractionDigits),
      "y1": a.y.toStringAsFixed(fractionDigits),
      "x2": b.x.toStringAsFixed(fractionDigits),
      "y2": b.y.toStringAsFixed(fractionDigits),
    };
}

LineElement tick(Point origin, num phi, num r1, num r2) {
  return line(a2c(origin, r1, phi), a2c(origin, r2, phi));
}

RectElement rectElement(Rectangle<int> r) => new RectElement()
  ..attributes = {
    "x": r.left.toString(),
    "y": r.top.toString(),
    "width": r.width.toString(),
    "height": r.height.toString(),
  };

CircleElement circleElem(Point<int> center, int r) => new CircleElement()
  ..attributes = {
    "cx": center.x.toString(),
    "cy": center.y.toString(),
    "r": r.toString()
  };

List<LineElement> cross(Point<num> p, num len, String cssClasses) {
  final v = new Point(0, len);
  final h = new Point(len, 0);
  return [
    line(p + v, p - v)..classes.addAll(cssClasses.split(' ')),
    line(p + h, p - h)..classes.addAll(cssClasses.split(' ')),
  ];
}

class Arc {
  final num r1, r2;
  Point? destination;

  int _sweep = 0;
  int _large = 0;
  double _rotation = 0.0;

  Arc.circle(this.r1) : r2 = r1 {}

  Arc(this.r1, this.r2) {}

  Arc to(Point p) {
    destination = p;
    return this;
  }

  /// To understand left() and right() best look at the sweep pictures here:
  /// https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
  Arc left() {
    _sweep = 1;
    return this;
  }

  /// Already the default.
  Arc right() {
    _sweep = 0;
    return this;
  }

  /// To understand large() and small() best look at the sweep pictures here:
  /// https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
  Arc large() {
    _large = 1;
    return this;
  }

  /// Already the default.
  Arc small() {
    _large = 0;
    return this;
  }

  /// Not needed for circles.
  /// https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
  Arc rotation(double rot) {
    _rotation = rot;
    return this;
  }

  String getRelative() {
    var d = destination;
    if (d == null) {
      throw new StateError("destination must be set");
    }
    return "a $r1 $r2, $_rotation, $_large $_sweep, ${d.x} ${d.y}";
  }

  String getAbsolute() {
    return getRelative().toUpperCase();
  }
}

String squarePath(Point control, Point dest) =>
    "S ${control.x} ${control.y} ${dest.x} ${dest.y}";

String cubicPath(Point c1, Point c2, Point dest) =>
    "C ${c1.x} ${c1.y} ${c2.x} ${c2.y} ${dest.x} ${dest.y}";

String p2s(Point p) => "${p.x} ${p.y}";

PathElement circleArc(Point origin, num r, num startSlot, num endSlot) {
  final start = "M " + p2s(a2c(origin, r, rad(startSlot)));
  String arc =
      new Arc.circle(r).large().to(a2c(origin, r, rad(endSlot))).getAbsolute();
  return new PathElement()..attributes = {"d": "$start $arc"};
}

// same function used for years and for months
TextElement makeLeapLabel(Point<num> origin, num r, num slot) {
  TextElement leapLabel = new TextElement()
    ..attributes = {
      "x": (origin.x - 0.065 * r).toStringAsFixed(2),
      "y": (origin.y - 0.965 * r).toStringAsFixed(2),
      "transform": rotateAttr(origin, slot + 0.45),
    }
    ..text = "Leap"
    ..classes.add("label-leap");
  return leapLabel;
}

List<SvgElement> makeYearWindow(Point origin, double rOut, double rIn) {
  final top = rOut + (rOut - rIn) / 4;
  final bottom = rIn;
  final left = -0.95;
  final right = -left;

  String start = "M " + p2s(a2c(origin, top, rad(left)));
  String outerArc = (new Arc.circle(top)
        ..left()
        ..to(a2c(origin, top, rad(right))))
      .getAbsolute();
  String down = "L " + p2s(a2c(origin, bottom, rad(right)));
  String innerArc = (new Arc.circle(bottom)..to(a2c(origin, bottom, rad(left))))
      .getAbsolute();

  final windowPath = new PathElement()
    ..attributes = {"d": "$start $outerArc $down $innerArc z"}
    ..classes.add("year-window");

  final cuttingLine = line(a2c(origin, top, 0), a2c(origin, bottom, 0))
    ..classes.add("dashed-line");

  final almostCircle = circleArc(origin, rOut, left, right)
    ..id = "months-outer-arc"
    ..classes.add("slim-rim");
  return [almostCircle, windowPath, cuttingLine];
}

// TODO: builder class for PathElement....
// class PathSegment {
//   @override
//   String toString() {
//     //
//   }
// }

String pathM(Point p) => "M ${p.x} ${p.y}";

// TODO: unit test after moving to lib/svg !!
PathElement makeFlapsicle(Point origin, num rIn, num rOut) {
  final p = (r, phi) => a2c(origin, r, rad(phi));
  // relative width of flapsicle window compared to one slot
  const w = 1.5;
  // relative width of flapsicle side compared to one slot
  const f = 0.75;

  // angle 0 is the center of slot 0, and we want to draw around slot 1 (= day 1)
  // so we move by one slot to the correct center and then back by half the window-width.
  const leftIn = 0.5 - w / 2;
  const leftOut = leftIn - f;
  const rightIn = leftIn + w;
  const rightOut = rightIn + f;

  final rTop = rOut + (rOut - rIn) * f;
  final path = [
    "M " + p2s(p(rIn, leftOut)),
    new Arc.circle(rIn).left().to(p(rIn, leftIn)).getAbsolute(),
    "L " + p2s(p(rOut, leftIn)),
    new Arc.circle(rOut).left().to(p(rOut, rightIn)).getAbsolute(),
    "L " + p2s(p(rIn, rightIn)),
    new Arc.circle(rIn).left().to(p(rIn, rightOut)).getAbsolute(),
    "L " + p2s(p(rTop, rightOut)),
    new Arc.circle(rTop).right().to(p(rTop, leftOut)).getAbsolute(),
    "z"
  ];
  return new PathElement()
    ..attributes = {"d": path.join(" ")}
    ..classes.add("flapsicle");
}
