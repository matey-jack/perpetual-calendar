/*
Adding holidays is interesting, because they are part of any complete calendar
(and also add some regional and cultural flavor). However, since many holidays
depend on Easter and this requires a wholly different mechanic, we can only add
half of all holidays, which is a bad user experience. Even if we strip it down
to a minimal set of holidays (for example, those shared by all european countries)
there will still be Christmas and Easter and it's weird, having one, but not the
other. Especially since everyone knows when Christmas is and can thus find the
days-of-week for 24th to 26th december, but Easter is not so easy to find.

Alternative idea: mark some other regular dates in the year, like beginning of the
season, which is conveniently always on the 21st of March, June, September, and
December, but then again, it is too easy and not practically relevant enough to
be worth it.

==> at least we can still make very personal calendar models with anniversaries
like birthdays, marriage etc. Even more profitable, haha!!

 */
import 'package:cal/model.dart';

// symmetrical relationship when same window (same day) or same mark
// asymmetrical relationship when A's window shows B's mark or the other way.
// "MY" refers to the target of the method call ('this', not 'other').
// Note that SAME_MARK is only save when both months are not Jan or Feb.
// If both are Jan or Feb and both marks can be the same (or only one), but in
// any case, there will be lots of conflicts between the leap and common marks.
// Basically a single anniversary in Jan or Feb can be seen as two different
// anniversaries sharing the same window...
enum ConflictType { SAME_DAY, SAME_MARK, MY_WINDOW, MY_MARK }

String leapSuffix(leap) => leap ? " (in leap years)" : "";

class Anniversary {
  // day and month are 1-based like in the real world and DateTime constants.
  final int day;
  final int month;

  const Anniversary(this.day, this.month);

  String toString() => "${getMonthName(month)} $day";

  int getMarker(bool leap) => getMonthStart(month, leap) + day;

  String? _checkMonthStart(int start, bool leap) {
    int candidate = getNearestMonth(start.toDouble(), leap);
    if (candidate != month && getMonthStart(candidate, leap) == start) {
      return "creates ${getMonthName(candidate)} $day${leapSuffix(leap)}";
    }
    return null;
  }

  String? _getMyWindowConflictDate(int otherMark) {
    int potentialStart = otherMark - day;
    return _checkMonthStart(potentialStart, false) ??
        _checkMonthStart(potentialStart, true);
  }

  /// Returns description of conflict or [null] for no conflict.
  /// TODO: rewrite this is getMyMarkerConflictDates on [other] calling in turn
  /// _getMyWindowConflictDate on its [other] and making the latter public again...
  String? getMyWindowConflictDate(Anniversary other) {
    var leapConflict = _getMyWindowConflictDate(other.getMarker(true));
    if (leapConflict != null && other.month <= DateTime.february)
      return "(leap marker) " + leapConflict;
    // if it's not a leap-year aware month, a positive result we repeat here:
    return _getMyWindowConflictDate(other.getMarker(false));
  }

  bool operator <(Anniversary other) =>
      other.month < month || (other.month == month && other.day < day);
}

String? anniversariesConflict(Anniversary a, Anniversary b) {
  var hin = a.getMyWindowConflictDate(b);
  var her = b.getMyWindowConflictDate(a);
  if (hin == null) return her;
  if (her == null) return hin;
  return "$hin and $her";
}

/*
Some faulty statistical analysis still gives rough idea how many conflicts to
expect.
Rough chance of conflict one-way is 13/35. (Excluding intended third anniversaries.)
Chance that no conflict either way is then (22/35)^2 = 39.5%.
Placing three random anniversaries (excluding the chance that two of them share
a window or mark) means that we need to be conflict-free for all three pairs
choosen out of the three anniversaries, so 39.5%^3 which leaves only 6% chance of
no conflict.
However, we have a better chance of at least two of the three anniversaries being
compatible so they can share a ring: 1 - 60.5%^3 = 77.8%.
Note that all of those are just estimates, because marks are not evenly distributed.
There are less marks around the year window and March/April, because not all month-
settings reach there.

Imagine, we reserve three rings for anniversaries, the chance that four random holidays
will fit there will be 86.6% which is still annoying when its the birthdays of your
family that you can't exactly tweak to make them fit. One way out of this would be to
switch calendar layout between 28, 35, and 42 slots or to swap position of leap months
(same or different side of year window) or to generally put the year window in a different
position or –in the 42 slot variant– add a second gap between months or put months in a
different order, such as alphabetical, haha!!
*/
