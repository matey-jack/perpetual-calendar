import 'dart:math';

/// helper methods which can be unit-tested without a browser)
/// (unlike SVG stuff which needs a browser)

/// convert angular coordinates to cartesian.
Point a2c(Point origin, num r, num phi) {
  return new Point(origin.x + r * sin(phi), origin.y - r * cos(phi));
}

// TODO: use this more instead of a2c and rad directly
class SlottedCircle<T extends num> {
  final Point<T> center;
  final T radius;

  SlottedCircle(this.center, this.radius);

  Point<num> point(num slot) => a2c(center, radius, rad(slot));
}

/// convert a slot index to radians.
double rad(x) => 2 * pi * x / 35;

String rotateAttr(Point origin, num slots) {
  return "rotate(${360 * slots / 35} ${origin.x}, ${origin.y})";
}

Point<T> box<T extends num>(Rectangle<T> r) => new Point(r.width, r.height);

Point<int> scale(Point<int> p, int mult, int div) =>
    new Point(p.x * mult ~/ div, p.y * mult ~/ div);

Point<int> scaleRelative(Point<int> p, Point<int> ref, int mult, int div) =>
    ref + scale(p - ref, mult, div);

Point<int> center(Rectangle<int> r) => r.topLeft + scale(box(r), 1, 2);

Point<T> perp<T extends num>(Point<T> p) => Point<T>(p.y, -p.x as T);

double sagitta(num r, num h) => 2 * sqrt(2 * r * h - h * h);

Point<T> mirrorH<T extends num>(Point<T> p) => Point<T>(p.x, -p.y as T);

Point<T> mirrorV<T extends num>(Point<T> p) => Point<T>(-p.x as T, p.y);
