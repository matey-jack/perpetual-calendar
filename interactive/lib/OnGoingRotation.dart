import 'dart:html';
import 'dart:math';

// yes, some global state in a library package
// let's see how far we can go with this
OngoingRotation? ongoingMouseRotation;
OngoingTouchRotation? ongoingTouchRotation;

abstract class RotatableValue {
  set val(double v);

  double get val;

  void draw(double v);

  drawCurrent() => draw(val);
}

/*
With touch events, updates (like 'move' and 'end') are sent as events to the
same element where the touch started, but the mouse event model has no such
concept. In order to allow a rotation where the mouse leaves the original
element, we need to listen to events on the surrounding (or better: containing)
elements. Since those event handlers are lacking the information on what is
currently rotating, we do need some global state.

Since this is so and to simplify touch and mouse logic by keeping it similar, we
use only one global state. (This means we can't support rotating several things
via multi-touch, but that isn't even on the longest list of backlog items
anyway.)
*/

class OngoingRotation {
  final Point _center;
  final RotatableValue _rv;
  double? _startAngle;

  OngoingRotation(RotatableValue this._rv, Point this._center);

  /// [getCenter] will be called on each start of rotation to determine the
  /// center of rotation.
  static void attachMouseListener(
      Element el, RotatableValue rv, Point Function() getCenter) {
    // start of method body ;)
    el.onMouseDown.listen((MouseEvent ev) {
      ev.preventDefault();
      if (ongoingMouseRotation != null) {
        print('ERROR: something is rotating already!');
      }
      ongoingMouseRotation = OngoingRotation(rv, getCenter())..start(ev.client);
    });
  }

  static void attachGlobalMouseListeners() {
    window.onMouseMove.listen((MouseEvent e) {
      ongoingMouseRotation?.rotate(e.client);
    });

    window.onMouseUp.listen((MouseEvent e) {
      ongoingMouseRotation?.end(e.client);
      ongoingMouseRotation = null;
    });
  }

  void start(Point p) {
    _startAngle = angleToOrigin(p);
  }

  void rotate(Point p) {
    _rv.draw(_rv.val - _getDeltaAngle(p));
  }

  void end(Point p) {
    _rv.val -= _getDeltaAngle(p);
    _rv.draw(_rv.val);
  }

  double _getDeltaAngle(Point p) {
    return (angleToOrigin(p) - _startAngle!) / pi / 2 * 35;
  }

  double angleToOrigin(Point p) {
    final dp = p - _center;
    return atan2(dp.x, dp.y);
  }
}

class OngoingTouchRotation {
  final OngoingRotation _rotation;
  int? _touchId;

  OngoingTouchRotation(RotatableValue rv, Point center)
      : _rotation = OngoingRotation(rv, center);

  /// [getCenter] will be called on each start of rotation to determine the
  /// center of rotation.
  static void attach(
      Element el, RotatableValue rv, Point Function() getCenter) {
    // start of method body ;)
    el.onTouchStart.listen((TouchEvent ev) {
      ev.preventDefault();
      if (ongoingTouchRotation != null) {
        print('ERROR: something is rotating already!');
      }
      ongoingTouchRotation = OngoingTouchRotation(rv, getCenter())..start(ev);
    });

    el.onTouchMove.listen((TouchEvent ev) {
      ongoingTouchRotation?.rotate(ev);
    });

    el.onTouchEnd.listen((TouchEvent ev) {
      ongoingTouchRotation?.end(ev);
      ongoingTouchRotation = null;
    });
  }

  void start(TouchEvent ev) {
    final Touch t = ev.changedTouches!.last;
    _touchId = t.identifier;
    _rotation.start(t.client);
  }

  void rotate(TouchEvent ev) => _rotation.rotate(_thisTouch(ev).client);

  void end(TouchEvent ev) => _rotation.end(_thisTouch(ev).client);

  Touch _thisTouch(TouchEvent ev) {
    return ev.changedTouches!.firstWhere((t) => t.identifier == _touchId);
  }
}
