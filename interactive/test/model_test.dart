@TestOn("vm")
import 'package:cal/model.dart';
import "package:test/test.dart";

void main() {
  group("years", () {
    test("getSlotForYear", () {
      expect(getSlotForYear(0), equals(0));
      expect(getSlotForYear(1), equals(1));
      expect(getSlotForYear(2), equals(2));
      expect(getSlotForYear(4), equals(5));
      expect(getSlotForYear(5), equals(6));
      expect(getSlotForYear(8), equals(10));
      expect(getSlotForYear(24), equals(30));
      expect(getSlotForYear(27), equals(33));
    });

    test("getNearestYear", () {
      expect(getNearestYear(0.1), equals(0));
      expect(getNearestYear(0.6), equals(1));
      expect(getNearestYear(3.9), equals(3));
      expect(getNearestYear(4.1), equals(4), reason: "4.1");
      expect(getNearestYear(4.6), equals(4), reason: "4.6");
      expect(getNearestYear(5.1), equals(4), reason: "5.1");
      expect(getNearestYear(6.0), equals(5));
      expect(getNearestYear(33.1), equals(27));

      expect(getNearestYear(0.1 - 35), equals(0), reason: "negative");
      expect(getNearestYear(4.6 - 35), equals(4), reason: "negative");
    });
  });

  group("getMonthStart", () {
    test("leap-year January", () {
      expect(getMonthStart(DateTime.january, true), equals(1));
    });

    test("common-year February", () {
      expect(getMonthStart(DateTime.february, false), equals(5));
    });

    test("April", () {
      expect(getMonthStart(DateTime.april, false), equals(15));
    });

    test("leap-year August", () {
      expect(getMonthStart(DateTime.august, true), equals(25));
    });

    test("December", () {
      expect(getMonthStart(DateTime.december, false), equals(35));
    });
  });

  group("getNearestMonth", () {
    test("just before March", () {
      expect(getNearestMonth(11.5, false), equals(DateTime.march));
    });

    test("just after February, common-year", () {
      expect(getNearestMonth(7.77, false), equals(DateTime.february));
    });

    test("just after January, leap-year", () {
      expect(getNearestMonth(2.4, true), equals(DateTime.january));
    });

    test("just before February, leap-year", () {
      expect(getNearestMonth(2.7, true), equals(DateTime.february));
    });

    test("after August, leap-year", () {
      expect(getNearestMonth(26.4, true), equals(DateTime.august));
    });

    test("negative slot after August, leap-year", () {
      expect(getNearestMonth(26.4 - 35, true), equals(DateTime.august));
    });

    test("spot-on december", () {
      expect(getNearestMonth(35.0, true), equals(DateTime.december));
    });

    test("before december", () {
      expect(getNearestMonth(34.0, true), equals(DateTime.december));
    });
  });
}
