@TestOn("vm")
import "package:test/test.dart";
import 'package:cal/math.dart';
import 'dart:math';

const EPS = 0.001;

class _DoubleMatcher extends Matcher {
  final _expected;

  const _DoubleMatcher(this._expected);

  bool matches(item, Map matchState) {
    return (item - _expected).abs() < EPS;
  }

  Description describe(Description description) =>
      description.add('equals approximately ').addDescriptionOf(_expected);

  Description describeMismatch(
      item, Description mmDesc, Map matchState, bool verbose) {
    return super.describeMismatch(item, mmDesc, matchState, verbose);
  }
}

Matcher equalsApprox(expected) => new _DoubleMatcher(expected);

void main() {
  group("Points", () {
    const Point o = const Point(12, 7);

    test("a2c straight down", () {
      expect(a2c(o, 10, 0), equals(const Point(12, -3)));
    });

    test("a2c 90 degrees", () {
      var result = a2c(o, 10, pi / 2);
      expect(result.x, equalsApprox(22));
      expect(result.y, equalsApprox(7));
    });

    test("a2c 225 degrees", () {
      var result = a2c(o, 10, pi + pi / 4);
      expect(result.x, equalsApprox(12 - 10 / sqrt(2)));
      expect(result.y, equalsApprox(7 + 10 / sqrt(2)));
    });
  });

  test("center", () {
    const r = const Rectangle<int>(12, 25, 40, 88);
    final result = center(r);
    expect(result.x, equals(32), reason: 'x');
    expect(result.y, equals(69), reason: 'x');
  });
}
