@TestOn("chrome")

import 'dart:math';
import 'dart:svg' hide Point;
import "package:test/test.dart";

import 'package:cal/svg.dart';
import 'math_test.dart';

void main() {
  group("tick", () {
    test("simple tick", () {
      final Point o = const Point(10, 0);
      final LineElement line = tick(o, pi / 4, 1, 2);
      expect(line.x1?.baseVal?.value, equalsApprox(10.707));
      expect(line.x2?.baseVal?.value, equalsApprox(11.414));
      expect(line.y1?.baseVal?.value, equalsApprox(-0.707));
      expect(line.y2?.baseVal?.value, equalsApprox(-1.414));
    });
  });

  group("arc", () {
    test("default circle", () {
      final result = new Arc.circle(5.0).to(new Point(1, 2)).getAbsolute();
      expect(result, equals("A 5 5, 0, 0 0, 1 2"));
    });

    test("custom ellipsis", () {
      final result = new Arc(5.0, 10.0)
          .left()
          .large()
          .rotation(20.5)
          .to(new Point(1, 2))
          .getAbsolute();
      expect(result, equals("A 5 10, 20.5, 1 1, 1 2"));
    });
  });

  group("arc'd window", () {
    test("simple window", () {
      SvgElement window = makeYearWindow(const Point(12, 20), 8.0, 6.0)[1];
      expect(
          window.getAttribute("d"),
          equals("M 10.557396242644314 11.623312444691322 "
              "A 8.5 8.5, 0, 0 1, 13.442603757355686 11.623312444691322 "
              "L 13.018308534604014 14.08704407860564 "
              "A 6 6, 0, 0 0, 10.981691465395986 14.08704407860564 "
              "z"));
    });
  });
}
