import 'dart:html';

void attachLookButtonHandlers() {
  final lookButton = querySelector("#look-button")!;
  lookButton.onMouseDown.listen(lookButtonDown);
  lookButton.onTouchStart.listen(lookButtonDown);
  lookButton.onMouseUp.listen(lookButtonUp);
  lookButton.onTouchEnd.listen(lookButtonUp);
}

void lookButtonDown(UIEvent e) {
  e.preventDefault();
  e.stopPropagation();
  querySelectorAll(".hider").classes.add("hidden");
  querySelectorAll("#months-group").classes.add("hidden");
  querySelectorAll("#daynumbers-group").classes.add("hidden");
}

void lookButtonUp(UIEvent e) {
  querySelectorAll(".hider").classes.remove("hidden");
  querySelectorAll("#months-group").classes.remove("hidden");
  querySelectorAll("#daynumbers-group").classes.remove("hidden");
}
