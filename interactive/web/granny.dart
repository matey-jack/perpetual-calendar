import 'dart:html';
import 'dart:svg' hide Point;

import 'package:cal/math.dart';
import 'package:cal/svg.dart' hide center;

final SvgElement svg = querySelector('svg')! as SvgElement;

const A4 = const Point<int>(210, 297);
final viewBox = scale(A4, 3, 2);

/// width for grabbing year disk which sticks out by this much.
const finger = 18;

/// cover overlaps year disk by this much at the closest point (top arc)
const coverOverlap = 2;

/// make the legs shorter but still align feet with bottom of paper
const topGap = 25;

const footWidth = 60;
// const footHeight = 18;
final legSize = 5 * footWidth ~/ 4;
final legAngle = 2 * footWidth ~/ 4;

final int rYears = A4.x ~/ 2;
final rMonthsCutout = rYears - 2 * finger;
final coverTopLeft = scale(viewBox - A4, 1, 2);
final coverRect =
    new Rectangle<int>.fromPoints(coverTopLeft, coverTopLeft + A4);
final navel = coverTopLeft + new Point(rYears, rYears + coverOverlap + topGap);

void main() {
  svg.setAttribute("viewBox", "0 0 ${viewBox.x} ${viewBox.y}");
  // note that in this version of SVG creation order = z-order

  // A4 bounding box of the cover (which itself is sculpted out of this)
  final coverRectElem = rectElement(coverRect)
    ..classes.add("dashed-line")
    ..attributes["id"] = "cover-rect";

  svg.children.add(coverRectElem);

  final CircleElement yearsOutmost = circleElem(navel, rYears)
    ..classes.addAll("slim-rim back-disk".split(' '))
    ..attributes["id"] = "years-outmost";
  svg.children.add(yearsOutmost);

  addWeekdays();

  svg.children.add(makeCover(coverRect));

  final CircleElement windowsOuterLimit = circleElem(navel, rYears - finger)
    ..classes.addAll("dashed-line".split(' '));
  svg.children.add(windowsOuterLimit);

  svg.children.addAll(cross(navel, 2, "slim-rim"));

  final CircleElement daynumberDisk = circleElem(navel, rMonthsCutout - 3)
    ..classes.addAll("daynumber-disk".split(' '));
  svg.children.add(daynumberDisk);
}

SvgElement makeCover(Rectangle<int> coverRect) {
  final heightOfCut = sagitta(rYears, finger).round();
  final topOfCut = navel.y - heightOfCut ~/ 2 - coverOverlap;
  final bottomOfCut = navel.y + heightOfCut ~/ 2 + coverOverlap;

  final leftFootRight = coverRect.bottomLeft + new Point(footWidth, 0);
  final rightFootLeft = coverRect.bottomRight - new Point(footWidth, 0);

  final bottomOfCutLeft = new Point(coverRect.left + finger, bottomOfCut);
  final bottomOfCutRight = new Point(coverRect.right - finger, bottomOfCut);

  final start = pathM(coverRect.bottomLeft);

  final x1 = coverRect.bottomRight + new Point(-legAngle, -legSize);
  final x2 = coverRect.bottomLeft + new Point(legAngle, -legSize);
  final c1 = bottomOfCutLeft + scale(perp(bottomOfCutLeft - navel), 1, 4);
  // final c2 = coverRect.bottomLeft + scale(mirrorH(c1 - bottomOfCutLeft), 1, 2);
  final leftWaist = cubicPath(x2, c1, bottomOfCutLeft);

  final upCut = "V ${topOfCut}";
  final around = new Arc.circle(rYears)
      .left()
      .to(new Point(coverRect.right - finger, topOfCut))
      .getAbsolute();
  final downCut = "V ${bottomOfCut}";

  final z1 = bottomOfCutRight - scale(perp(bottomOfCutRight - navel), 1, 4);
  // final z2 = coverRect.bottomRight + scale(mirrorH(z1 - bottomOfCutRight), 1, 2);
  final rightWaist = cubicPath(z1, x1, coverRect.bottomRight);

  final rightFoot = "H ${rightFootLeft.x}";

  final legs = cubicPath(x1, x2, leftFootRight);

  final windowTopLeft = a2c(navel, rYears - finger, rad(-1));
  final windowTopRight = a2c(navel, rYears - finger, rad(1));
  final windowBottomRight = a2c(navel, rMonthsCutout, rad(1));
  final windowBottomLeft = a2c(navel, rMonthsCutout, rad(-1));
  final goInside = pathM(windowTopLeft);
  final windowLeft = "L " + p2s(windowBottomLeft);
  final innerCircle =
      new Arc.circle(rMonthsCutout).to(windowBottomRight).large().getAbsolute();
  final windowRight = "L " + p2s(windowTopRight);
  final windowArc =
      new Arc.circle(rYears - finger).to(windowTopLeft).getAbsolute();

  final backOutside = pathM(leftFootRight);
  final close = "H ${coverRect.left}";

  return new PathElement()
    ..attributes = {
      "d": "$start $leftWaist $upCut $around "
          "$downCut $rightWaist $rightFoot $legs $goInside "
          "$windowLeft $innerCircle $windowRight $windowArc $backOutside $close"
    }
    ..classes.addAll("slim-rim cover".split(' '));
}

void addWeekdays() {
  final circle = new SlottedCircle<num>(navel, rMonthsCutout - 1.5);
  for (var week = 0; week < 5; week++) {
    final sunday = week * 7;
    LineElement segment(num slotStart, num slotEnd) =>
        line(circle.point(sunday + slotStart), circle.point(sunday + slotEnd));
    svg.children.add(segment(2, 3)..classes.add("weekday-line"));
    svg.children.add(segment(3, 4.4)..classes.add("weekday-line"));
    svg.children.add(segment(4.6, 6)..classes.add("weekday-line"));
    svg.children.add(segment(6, 7)..classes.add("weekday-line"));
//    svg.children.add(line(circle.point(saturday), circle.point(saturday + 1))
//      ..classes.add("weekend-line"));
//    svg.children.add(line(circle.point(saturday +1), circle.point(saturday + 2))
//      ..classes.add("weekend-line"));
//    svg.children.add(line(circle.point(saturday + 4), circle.point(saturday + 5))
//      ..classes.add("wednesday-line"));

//    for (var day in [2, 3, 5, 6]) {
//      svg.children.add(
//          line(circle.point(sunday + day), circle.point(sunday + day + 1))
//            ..classes.add("weekday-line"));
//    }
  }
}
