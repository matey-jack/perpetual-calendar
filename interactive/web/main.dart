import 'dart:html';
import 'dart:math';
import 'dart:svg' hide Point;

import 'package:cal/math.dart' hide center;
import 'package:cal/model.dart';
import 'package:cal/svg.dart';
import 'package:cal/OnGoingRotation.dart';

import 'look_button.dart';

// TODO: would be nice to only use this constant in CalendarState, but we need
// it for SVG creation, too. Maybe add another layer of abstraction in-between?
const _JANUARY_SLOT = -9;

class CalendarState {
  int year;
  int month;

  CalendarState(DateTime date)
      : year = date.year,
        month = date.month;

  bool isLeap() => isLeapYear(year);

  // we measure rotation in slots
  double get daynumbersRotation =>
      getMonthStart(month, isLeap()).toDouble() + _JANUARY_SLOT;

  set daynumbersRotation(double rotation) {
    month = getNearestMonth(rotation - _JANUARY_SLOT, isLeap());
  }

  static const _CENTURY = 2000;

  double get yearDiskRotation => -getSlotForYear(year - _CENTURY).toDouble();

  set yearDiskRotation(double rotation) {
    year = getNearestYear(-rotation) + _CENTURY;
  }
}

final calendarState = CalendarState(DateTime.now());

class MonthsRotatableValue extends RotatableValue {
  get val {
    return calendarState.daynumbersRotation;
  }

  set val(v) {
    calendarState.daynumbersRotation = v;
  }

  draw(v) {
    rotateSelector('#daynumbers-group', v);
  }
}

final monthsRotatableValue = MonthsRotatableValue();

class YearsRotatableValue extends RotatableValue {
  get val {
    return calendarState.yearDiskRotation;
  }

  set val(v) {
    calendarState.yearDiskRotation = v;

    // update in case of leap month and change of leap year status.
    monthsRotatableValue.draw(monthsRotatableValue.val);
  }

  draw(v) {
    rotateSelector('#years-group', v);
    rotateSelector('#weekdays-group', v);
  }
}

final yearsRotatableValue = YearsRotatableValue();

double angleToOrigin(Point p) {
  final dp = p - getOriginInPage();
  return atan2(dp.x, dp.y);
}

// Always get this fresh, because it changes with resizes (and, possibly, zoom).
Point getOriginInPage() {
  final offset = querySelector('#calendar-container')!.offset;
  return (offset.topLeft + offset.bottomRight) * 0.5;
}

// need that in event handlers
final Point origin =
    center(querySelector('#weekdays-outer-ring')! as CircleElement)!;

void main() {
  attachLookButtonHandlers();

  final CircleElement weekdayRim =
      querySelector('#weekdays-outer-ring')! as CircleElement;
  final num rDays = radius(weekdayRim)!;
  final weekdaysGroup = querySelector('#weekdays-group') as SvgElement;
  querySelector('#rotated-hider')!.attributes["transform"] =
      rotateAttr(origin, 1.0);
  addWeekdays(origin, rDays, weekdaysGroup);

  final rMonthsOut = 47.0;
  final rMonthsIn =
      radius(querySelector('#months-inner-ring')! as CircleElement)!;
  final monthGroup = querySelector('#months-group')! as SvgElement;
  MonthRingGenerator(origin, rMonthsOut, rMonthsIn.toDouble())
      .generate(monthGroup);

  final yearsGroup = querySelector('#years-group') as SvgElement;
  addYears(origin, rMonthsOut, yearsGroup);

  OngoingTouchRotation.attach(
      yearsGroup, yearsRotatableValue, getCenterInEventCoordinates);
  OngoingRotation.attachMouseListener(
      yearsGroup, yearsRotatableValue, getCenterInEventCoordinates);

  final daynumbersGroup = querySelector('#daynumbers-group')! as SvgElement;
  final rDaysOut =
      radius(querySelector("#daynumbers-outer-ring")! as CircleElement)!;
  addDaynumbers(origin, rDays, daynumbersGroup);
  daynumbersGroup.children.add(makeFlapsicle(origin, rDaysOut, rMonthsOut));

  OngoingTouchRotation.attach(
      daynumbersGroup, monthsRotatableValue, getCenterInEventCoordinates);
  OngoingRotation.attachMouseListener(
      daynumbersGroup, monthsRotatableValue, getCenterInEventCoordinates);

  final svgContainer = querySelector('#calendar-container')!;
  querySelector('#second-page')!
      .children
      .add(svgContainer.clone(true) as Element);
  querySelector('#third-page')!
      .children
      .add(svgContainer.clone(true) as Element);
  querySelector('#fourth-page')!
      .children
      .add(svgContainer.clone(true) as Element);

  OngoingRotation.attachGlobalMouseListeners();
  yearsRotatableValue.drawCurrent();
  monthsRotatableValue.drawCurrent();
}

Point getCenterInEventCoordinates() {
  final offset = querySelector('#calendar-container')!.offset;
  return (offset.topLeft + offset.bottomRight) * 0.5;
}

void rotateSelector(String selector, double slots) {
  querySelector(selector)!.attributes["transform"] = rotateAttr(origin, slots);
}

void addYears(Point<num> origin, num r, SvgElement svg) {
  for (int year = 0; year < 28; year++) {
    double slot = getSlotForYear(year) + 0.5;
    TextElement labelYear = TextElement()
      ..attributes = {
        "x": (origin.x - 0.075 * r).toStringAsFixed(2),
        "y": (origin.y - 0.895 * r).toStringAsFixed(2),
        "transform": rotateAttr(origin, slot),
      }
      ..text = year.toString().padLeft(2, '0')
      ..classes.add("label-year");
    if (year % 4 == 0) {
      svg.children.add(makeLeapLabel(origin, r * 1.035, slot));
      labelYear.classes.add("leap-year");
    }
    svg.children.add(labelYear);
  }
}

/*
Hohler Ring zum anfassen:
    <path d="M10,10h100v100h-100zM20,20v80h80v-80z" fill="yellow" stroke="black"/>
Die innen/außen Erkennung funktioniert wegen der "even/odd fill rule".
 */

void addWeekdays(Point<num> origin, num r, SvgElement svg) {
  for (int i = 0; i < 35; i++) {
    final phi = 2 * pi * i / 35;
    LineElement t;
    if (indexToWeekday(i) % 7 == 0 || indexToWeekday(i) % 7 == 5) {
      t = tick(origin, phi, 0.9 * r, r);
      t.classes.add("big-tick");
    } else {
      t = tick(origin, phi, 0.95 * r, r);
      t.classes.add("tick");
    }
    svg.children.add(t);
    TextElement labelDay = TextElement();
    labelDay.attributes = {
      "x": (origin.x - 0.065 * r).toStringAsFixed(2),
      "y": (origin.y - 0.92 * r).toStringAsFixed(2),
      "transform": rotateAttr(origin, i + 0.5),
    };
    labelDay.text = weekdays[indexToWeekday(i)];
    labelDay.classes.add("label-day");
    if (indexToWeekday(i) > 4) {
      labelDay.classes.add("label-day-weekend");
    }
    svg.children.add(labelDay);
  }
}

/// Adjusting constant for weekdays to fit years.
// This is important, because years and week-days are in the same
// SVG group so we don't need any more tuning, as they will rotate
// together.
int indexToWeekday(int i) => (i + 6) % 7;

void addDaynumbers(Point<num> origin, num r, SvgElement svg) {
  for (int i = 1; i <= 31; i++) {
    TextElement labelDay = TextElement();
    labelDay.attributes = {
      "x": (origin.x - 0.065 * r).toStringAsFixed(2),
      "y": (origin.y - 1.035 * r).toStringAsFixed(2),
      "transform": rotateAttr(origin, i - 0.5),
    };
    labelDay.text = i.toString();
    labelDay.classes.add("label-daynumber");
    svg.children.add(labelDay);
  }
}

class MonthRingGenerator {
  final Point<num> origin;
  final double outerRadius;
  final double innerRadius;
  final double smallTickRadius;

  MonthRingGenerator(this.origin, this.outerRadius, this.innerRadius)
      : smallTickRadius = outerRadius - (outerRadius - innerRadius) / 3;

  void generate(SvgElement svg) {
    for (int month = 1; month <= 12; month++) {
      final slot = getMonthStart(month, false) + _JANUARY_SLOT;
      if (month <= 2) {
        _addMonthLabel(svg, month, slot - 1, true);
      }
      _addMonthLabel(svg, month, slot, false);
      _addSmallTicks(svg, slot + 1, month);
    }

    svg.children.add(_makeCentury(origin, outerRadius));
    svg.children.addAll(makeYearWindow(origin, outerRadius, innerRadius));
  }

  void _addMonthLabel(SvgElement svg, int month, int slot, bool isLeap) {
    svg.children.add(_makeBigTick(slot));
    svg.children.add(_makeMonthLabel(slot, getMonthName(month), isLeap));
    if (isLeap) {
      svg.children.add(makeLeapLabel(origin, outerRadius, slot + 0.5));
    }
  }

  void _addSmallTicks(SvgElement svg, int startSlot, int month) {
    int nextMonth = month == 12 ? 1 : month + 1;
    // always end before leap month since that is the earlier one.
    int endSlot = getMonthStart(nextMonth, true) + _JANUARY_SLOT;
    for (num slot = startSlot; slot < endSlot; slot++) {
      // skip ticks inside year window
      if (slot >= -1 && slot <= 1) continue;
      svg.children.add(_makeSmallTick(slot));
    }
  }

  SvgElement _makeMonthLabel(num slot, String name, bool isLeap) {
    TextElement labelMonth = TextElement();
    labelMonth.attributes = {
      "x": (origin.x - 0.067 * outerRadius).toStringAsFixed(2),
      "y": (origin.y - 0.9 * outerRadius).toStringAsFixed(2),
      "transform": rotateAttr(origin, slot + 0.5),
    };
    labelMonth.text = name;
    labelMonth.classes.add("label-month");
    if (isLeap) {
      labelMonth.classes.add("leap-month");
    }
    return labelMonth;
  }

  SvgElement _makeBigTick(num slot) {
    LineElement t = tick(origin, rad(slot), innerRadius, outerRadius);
    t.classes.add("big-tick");
    return t;
  }

  SvgElement _makeSmallTick(num slot) {
    LineElement t = tick(origin, rad(slot), smallTickRadius, outerRadius);
    t.classes.add("tick");
    return t;
  }

  TextElement _makeCentury(Point<num> origin, num r) {
    TextElement labelCentury = TextElement()
      ..attributes = {
        "x": (origin.x - 0.055 * r).toStringAsFixed(2),
        "y": (origin.y - 0.895 * r).toStringAsFixed(2),
        "transform": rotateAttr(origin, -0.5),
      }
      ..text = "20"
      ..classes.add("label-century");
    return labelCentury;
  }
}
